function [X, Y, rk] = preprocess_data(table_data, ratio, data_type)
%PREPROCESS_DATA Preprocess the data in the adult dataset
%
%   input -----------------------------------------------------------------
%
%       o table_data    : (M x N), a cell array containing mixed
%                         categorical and continuous values
%       o ratio : The pourcentage of M samples to extract 
%
%   output ----------------------------------------------------------------
%       o X : (N-1, M*ratio) Data extracted from the table where
%             categorical values are converted to integer values
%       o Y : (1, M*ratio) The label of the data to classify. Values are 1
%             or 2

% Auxiliary Variables
rk = zeros(size(table_data,2),1);
nbSamples = floor(size(table_data,1)*ratio);
% take only a sub sample of the data to reduce computational cost
idx = randperm(size(table_data, 1), nbSamples);
X = zeros(size(table_data,2)-1, nbSamples);

% ADD CODE HERE: Convert features data to numerical values. If the data are 
% categorical first convert them to int values. If the data are continuous 
% store the range in rk.
% HINT: Type of feature data (continuous or categorical) is stored in
% data_type which is boolean cell array (true if continuous). Only select
% the samples based on idx array. Be careful with the input and output 
% dimensions.

[~, N] = size(table_data);


for k=1:N-1
    if data_type{k}
        rk(k) = max(table_data{idx, k}) - min(table_data{idx, k});
        X(k, :) = table_data{idx, k};
    else
        X(k, :) = grp2idx(table_data{idx, k});
    end
end


Y(1, :) = grp2idx(table_data{idx, N});

% END CODE
end

