function [S] = gower_similarity(X1,X2,data_type, rk)
%GOWER_SIMILARITY Compute the Gower similarity between X1 and X2
%
%   input -----------------------------------------------------------------
%       o X1 : (N x 1) First sample point
%       o X2 : (N x 1) Second sample point
%       o data_type : {N x 1}, a boolean cell array with true when
%                     feature is continuous
%       o rk : (N x 1) The range of values for continuous data 
%
%   output ----------------------------------------------------------------
%      

% Auxiliary variables
N = size(X1,1);
Si = zeros(N,1);

% ADD CODE HERE: For each feature i of N calculate the Gower similarity. At
% the end sum it to calculate the similarity between the tzo samples.
% HINT: The formula varies depending if the feature is catagorical or
% continuous.


for k=1:N
    if data_type{k}
        Si(k) = 1 - (abs(X1(k) - X2(k))) / rk(k);
    else
        Si(k) = int8(X1(k) == X2(k));
    end
end

% continous = (cell2mat(data_type) == true);
% categorical = (cell2mat(data_type) == true);
% Si(continous) = 1 - (abs(X1(continous) - X2(continous))) ./ rk(continous);
% Si(categorical) = int8(X1(categorical) == X2(categorical));

S = sum(Si) / N;

% END CODE

