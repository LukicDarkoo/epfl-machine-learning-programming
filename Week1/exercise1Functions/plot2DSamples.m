function [] = plot2DSamples(data, classes, featuresLabel, axis)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

% ADD CODE HERE: extract the dimensions given in axis (2 lines)


% END CODE

% create a different plot depending on the given axis
[r,c] = size(axis);

xaxis = axis(1);
if c > 1
    yaxis = axis(2);
end
    
% different feature case
if (r > 1) || (c > 1)
    % ADD CODE HERE: plot each sample of the three species (1 line)
    % HINT: look for the function gscatter
    gscatter(data(:, xaxis), data(:, yaxis), classes)

    % END CODE

    xlabel(featuresLabel(1));
    ylabel(featuresLabel(2));
    grid on

% histogram case
else
    % set a color for each class
    colors = ['b','g','r'];
    
    % ADD CODE HERE: extract classes names (return an array of the 3 names)
    classNames = unique(classes);
    % END CODE
    
    % ADD CODE HERE: encode the Y vector (each label corresponding to an int)
    % classes = [1, 2, 3]
    % END CODE
    
    % plot a different histogram for each class
    for i=1:length(classNames)
        % ADD CODE HERE: extract the indexes corresponding to the i-th class
        
        indexes = strcmp(classNames(i), classes);
        vals = data(indexes, xaxis);
        histogram(vals)
        % END CODE
        
        % ADD CODE HERE: plot an histogram of the corresponding samples
        
	% END CODE
        hold on
    end
    
    % add the legend and axis title
    legend(classNames) 
    xlabel(featuresLabel);
    ylabel('Counts')
end
end

