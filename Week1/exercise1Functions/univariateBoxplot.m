function [] = univariateBoxplot(data, classes, featuresLabel)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% extract the number of dimemsions from the data
dim = size(data);

% create a subplot for each features (second dimension) and draw a boxplot
% of the data

for i=1:dim(2)
    subplot(dim(2)/2, dim(2)/2, i)
    
    
    % ADD CODE HERE: plot the boxplot of the i-th feature (1 line)
    
    %indexes = strfind(classes, 'setosa')
    %indexes = find(contains(classes,'setosa'))
    %data(indexes, i)
    %for class=classes(:)
    %    class
    boxplot(data(:, i), classes)
    %boxplot([data(1:50, i), data(51:100, i), data(101:150, i)], 'Labels', { 'setosa', 'versicolor', 'virginica' })
    % END CODE
    
    xlabel('Species')
    ylabel(featuresLabel(i))
end

end

