function [Priors,Mu,Sigma] = maximization_step(X, Pk_x, params)
%MAXIMISATION_STEP Compute the maximization step of the EM algorithm
%   input------------------------------------------------------------------
%       o X         : (N x M), a data set with M samples each being of 
%       o Pk_x      : (K, M) a KxM matrix containing the posterior probabilty
%                     that a k Gaussian is responsible for generating a point
%                     m in the dataset, output of the expectation step
%       o params    : The hyperparameters structure that contains k, the number of Gaussians
%                     and cov_type the coviariance type
%   output ----------------------------------------------------------------
%       o Priors    : (1 x K), the set of updated priors (or mixing weights) for each
%                           k-th Gaussian component
%       o Mu        : (N x K), an NxK matrix corresponding to the updated centroids 
%                           mu = {mu^1,...mu^K}
%       o Sigma     : (N x N x K), an NxNxK matrix corresponding to the
%                   updated Covariance matrices  Sigma = {Sigma^1,...,Sigma^K}
%%

% Additional variables
[N, M] = size(X);
Priors = zeros(1,params.k);
Mu = zeros(N,params.k);
Sigma = zeros(N,N,params.k);
eps = 1e-5;


Priors = (1/M) .* sum(Pk_x, 2);
Priors = Priors';

% TODO: Improve
for k = 1:params.k
    Mu(:,k) = sum(X .* Pk_x(k,:), 2) / sum(Pk_x(k, :));
    X_Mu = X - Mu(:, k);
    
    if strcmp(params.cov_type, 'full')
        numer = (X_Mu .* Pk_x(k, :)) * X_Mu';
        denom = sum(Pk_x(k, :));
        Sigma(:, :, k) = numer / denom; 
    elseif strcmp(params.cov_type, 'diag')
        numer = (X_Mu .* Pk_x(k, :)) * X_Mu';
        denom = sum(Pk_x(k, :));
        Sigma(:,:,k) = diag(diag(numer / denom));
    elseif strcmp(params.cov_type, 'iso')
        numer = zeros(N, N);
        for m = 1:M
            numer = numer + Pk_x(k, m) * ( X(:,m) - Mu(:,k) )' * (X(:, m) - Mu(:,k)) * diag(ones(N, 1));
        end
        Sigma(:, :, k) = numer / (N * sum(Pk_x(k, :)));
    end

    Sigma(:, :, k) = Sigma(:, :, k) + diag(ones(N, 1)) * eps;
end    

end

