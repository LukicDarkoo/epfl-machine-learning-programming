function [models] = my_gmm_models(Xtrain, Ytrain, labels, params)

models = repmat(struct, length(labels), 1);

for c = 1:length(labels)
    y = labels(c);
    
    [ Priors, Mu, Sigma, ~, ~, ~, ~ ] = my_gmmEM(Xtrain(:, Ytrain == y), params);
    
    models(c).Priors = Priors;
    models(c).Mu = Mu;
    models(c).Sigma = Sigma;
end

end

