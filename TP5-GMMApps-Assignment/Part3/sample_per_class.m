function X = sample_per_class(models, class, nbPoints)

[~, K] = size(models(1).Mu);
X = [];

for k=1:K
    tmp = mvnrnd(models(class).Mu(:, k), models(class).Sigma(:, :, k), nbPoints / K);
    X = [ X; tmp ];
end
X = X';


end

