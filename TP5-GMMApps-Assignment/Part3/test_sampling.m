%% DO NOT MODIFY THIS UNLESS YOU ARE ON YOUR OWN COMPUTER
addpath(genpath("../../Repositories/ML_toolbox/")) % TODO CHANGE FOR
%WINDOWS LOCATION

addpath(genpath("../utils"))
addpath(genpath("../Part1"))

clear; 
close all; 
clc;

dataset_path = '../../TP5-GMMApps-Datasets/';

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%         1) Sampling from 2D Dataset
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nbPoints = 1000;
%% ADD CODE HERE
% Load a dataset, and train GMM model and sample nbPoints points from it

data = halfkernel();
X = data(:, 1:2)';
Y = data(:, 3);
K = 4;


[ Xtrain, Ytrain, Xtest, Ytest ] = split_regression_data(X, Y, 0.1);

params.cov_type = 'full';
params.k = K;
params.max_iter_init = 100;
params.max_iter = 500;
params.d_type = 'L2';
params.init = 'plus';
labels = unique(Y);
[models] = my_gmm_models(Xtrain, Ytrain, labels, params);

% A bit complex, I expected that we need to create datepoints for two classes separetely
XNew = [];
K_all = length(labels) * K;
for c=1:length(labels)
    for k=1:K
        tmp = mvnrnd(models(c).Mu(:, k), models(c).Sigma(:, :, k), nbPoints / K_all);
        XNew = [ XNew; tmp ];
    end
end
XNew = XNew';






%% END CODE

% plot both the original data and the sampled ones
figure('Name', 'Original dataset')
dotsize = 12;
ax1 = subplot(1,2,1);
scatter(X(1,:), X(2,:), dotsize);
title('Original Data')
ax2 = subplot(1,2,2);
scatter(XNew(1,:), XNew(2,:), dotsize);
title('Sampled Data')
linkaxes([ax1,ax2],'xy')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%         2) Sampling from high-dimensional data
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

train_data = csvread('mnist_train.csv', 1, 0);  
nbSubSamples = 2000;
idx = randperm(size(train_data, 1), nbSubSamples);
train_data = train_data(idx,:);

% extract the data
Xtrain = train_data(:,2:end)';
Ytrain = train_data(:,1)';

plot_digits(Xtrain)

p = 40; % in cases you need this, use this value

%% ADD CODE HERE


[ V, L, Mu ] = compute_pca(Xtrain);
[A_p, XtrainReduced] = project_pca(Xtrain, Mu, V, p);
[XHat] = reconstruct_pca(XtrainReduced, A_p, Mu);


% plot the reconstructed

plot_digits(XHat)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%         1) Sampling from a GMM per class
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ADD CODE HERE
% we can also train different models of GMM for each class of digits
params.cov_type = 'full';
params.k = 20;
params.max_iter_init = 100;
params.max_iter = 500;
params.d_type = 'L2';
params.init = 'plus';
labels = unique(Ytrain);
[models] = my_gmm_models(XtrainReduced, Ytrain, labels, params);
% sample fron the GMM that are trained on this specific digit
class = 5;
X = sample_per_class(models, class, 100);

% reconstruct the images from the PCA
XHat = reconstruct_pca(X, A_p, Mu);
%% END CODE

% plot the reconstructed
plot_digits(XHat)