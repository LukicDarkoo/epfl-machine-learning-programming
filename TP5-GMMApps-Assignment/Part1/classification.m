function [y_est] = classification(labels, Xtest, models, K)

M = size(Xtest, 2);
C = length(labels);

probs = zeros(C, M);
y_est = zeros(1, M);

for c = 1:C
    for k=1:K
        % test = my_gaussPDF(Xtest, models(c).Mu(k), models(c).Sigma(k));
        probs(c, :) = probs(c, :) + my_gaussPDF(Xtest, models(c).Mu(:, k), models(c).Sigma(:, :, k));
    end
end
for i = 1:M
    [~, c_index] = min(-log(probs(:, i)));
    y_est(1, i) = labels(c_index);
end

end

