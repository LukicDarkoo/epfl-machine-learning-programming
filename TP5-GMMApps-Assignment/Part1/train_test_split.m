function [X_train, Y_train, X_test, Y_test] = train_test_split(X, Y, validSize, seed)
%trainTestSplit Split a dataset between train and test data

% Re-apply the seed
rng(seed)

% Encode the Y vector (each label corresponding to an int)
Y = grp2idx(Y);

% Calculate the number of elements
nbSamples = size(X, 1);

% ADD CODE HERE: Shuffle both X and Y in the same way
% HINT: look up the function randperm in the documentation and indexing in
% the tips file.
rand_perm = randperm(nbSamples);
Y = Y(rand_perm);
X = X(rand_perm, :);


% END CODE

% calculate the number of classes
nbClasses = length(unique(Y));

% ADD CODE HERE: Calculate the number of train and test samples from valid_size
% HINT: be careful about the meaning of validSize. It is the ratio of
% samples in the TEST set.
nbTrainSamples = round(nbSamples * (1 - validSize));
nbTestSamples = nbSamples - nbTrainSamples;
% END CODE

% Calculate the number of train samples per classes based on their
% repartition on the original data
for i=1:nbClasses
    % ADD CODE HERE: calculate the class repartition in Y
    % HINT: it corresponds to the ratio of elements of the i-th class in
    % the whole dataset. Consider using the function sum over a logical
    % array
    classRepartition = sum(Y == i) / nbSamples;
    % END CODE
    
    % multiply the classRepartition ratio with both number of samples
    nbTrainSamplesPerClass(i) = nbTrainSamples * classRepartition;
    nbTestSamplesPerClass(i) = nbTestSamples * classRepartition;
end


% ADD CODE HERE: Initialize an array with zeros to keep track of the
% current number of samples per class
currentTrainSamplesPerClass = zeros(nbClasses, 1);
currentTestSamplesPerClass = zeros(nbClasses, 1);

% Initialize all returned arrays to empty vectors
X_train = [];
Y_train = [];
X_test = [];
Y_test = [];

% Split the dataset between train and test set
for i=1:nbSamples
    % Get the class of the sample
    class = Y(i);
    % ADD CODE HERE Add the element in the train set if you have not reached the desired
    % number of train or test sample for this class
    if currentTrainSamplesPerClass(class) < nbTrainSamplesPerClass(class)
        X_train = cat(1, X_train, [X(i,:)]);
        Y_train = cat(1, Y_train, [class]);
        currentTrainSamplesPerClass(class) = currentTrainSamplesPerClass(class) + 1; 
    else
        X_test = cat(1, X_test, [X(i,:)]);
        Y_test = cat(1, Y_test, [class]);
        currentTestSamplesPerClass(class) = currentTestSamplesPerClass(class) + 1;
    end
    % END CODE
end
