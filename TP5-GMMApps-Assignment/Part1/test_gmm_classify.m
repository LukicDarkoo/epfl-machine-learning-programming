%% DO NOT MODIFY THIS UNLESS YOU ARE ON YOUR OWN COMPUTER
addpath(genpath("../../ML_toolbox"))
%WINDOWS LOCATION

addpath(genpath("../utils"))

clear; 
close all; 
clc;

dataset_path = '../../TP5-GMMApps-Datasets/';

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%         1) GMM-Classification 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ADD CODE HERE

% Load a dataset, and train GMM models
% You can use any of the dataset functions provided in the 
% dataset folder in utils


data = halfkernel();
X = data(:, 1:2)';
Y = data(:, 3);
K = 4;
models = repmat(struct, length(unique(Y)), 1);

[ Xtrain, Ytrain, Xtest, Ytest ] = split_regression_data(X, Y, 0.1);


labels = unique(Y);
for c = 1:length(labels)
    y = labels(c);
    
    params.cov_type = 'full';
    params.k = K;
    params.max_iter_init = 100;
    params.max_iter = 500;
    params.d_type = 'L2';
    params.init = 'plus';
    
    [ Priors, Mu, Sigma, iter, PriorsList, MuList, SigmaList ] = my_gmmEM(Xtrain(:, Ytrain == y), params);
    
    models(c).Priors = Priors;
    models(c).Mu = Mu;
    models(c).Sigma = Sigma;
end

%% END CODE

% visualize the GMM fitting
figure('Name', 'Original dataset')
dotsize = 12;
scatter(data(:,1), data(:,2), dotsize, data(:,3)); axis equal;

% Display contours for each class
for c = 1:length(unique(Y))
    ml_plot_gmm_pdf(Xtrain, models(c).Priors, models(c).Mu, models(c).Sigma)
    hold off 
end

%% ADD CODE HERE
% Perform classification on the testing set
y_est = classification(labels, Xtest, models, K);


%% END CODE 

% Compute Accuracy
acc =  my_accuracy(Ytest', y_est)

% visualize it
figure('Name', 'Classification with GMM')
ax1 = subplot(1,2,1);
dotsize = 12;
scatter(data(:,1), data(:,2), dotsize, data(:,3)); axis equal;
title('Original Data');

% Plot decision boundary
ax2 = subplot(1,2,2);
plot_boundaries(Xtrain, Ytrain, Xtest, Ytest, y_est,  models, params.k);
linkaxes([ax1,ax2],'xy')