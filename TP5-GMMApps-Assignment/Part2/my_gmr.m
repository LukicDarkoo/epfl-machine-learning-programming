function [y_est, var_est] = my_gmr(Priors, Mu, Sigma, X, in, out)
%MY_GMR This function performs Gaussian Mixture Regression (GMR), using the 
% parameters of a Gaussian Mixture Model (GMM) for a D-dimensional dataset,
% for D= N+P, where N is the dimensionality of the inputs and P the 
% dimensionality of the outputs.
%
% Inputs -----------------------------------------------------------------
%   o Priors:  1 x K array representing the prior probabilities of the K GMM 
%              components.
%   o Mu:      D x K array representing the centers of the K GMM components.
%   o Sigma:   D x D x K array representing the covariance matrices of the 
%              K GMM components.
%   o X:       N x M array representing M datapoints of N dimensions.
%   o in:      1 x N array representing the dimensions of the GMM parameters
%                to consider as inputs.
%   o out:     1 x P array representing the dimensions of the GMM parameters
%                to consider as outputs. 
% Outputs ----------------------------------------------------------------
%   o y_est:     P x M array representing the retrieved M datapoints of 
%                P dimensions, i.e. expected means.
%   o var_est:   P x P x M array representing the M expected covariance 
%                matrices retrieved. 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


[N, M] = size(X);
[~, K] = size(Mu);
[~, P] = size(out);

var_est = zeros(P, P, M);
probs_xx = zeros(K, M);
Mu_tild = zeros(P,M,K);
y_est = zeros(P,M);
Sigma_tild = zeros(P, P, K);

for k=1:K
    probs = my_gaussPDF(X, Mu(1:N, k), Sigma(1:N, 1:N, k));
    probs_xx(k, :) = Priors(k) * probs;    
end
beta_k = probs_xx ./ sum(probs_xx, 1);

Mu_y = Mu(N + 1:end, :);
Mu_x = Mu(1:N, :);
Sigma_xx = Sigma(1:N, 1:N, :);
Sigma_xy = Sigma(1:N, N+1:end, :);
Sigma_yx = Sigma(N+1:end, 1:N, :);
Sigma_yy = Sigma(N+1:end, N+1:end, :);

% µ˜k(x)
for k = 1:K
    Mu_tild(:, :, k) = Mu_y(:, k) + Sigma_yx(:, :, k) * inv(Sigma_xx(:, :, k)) * (X - Mu_x(:, k));
end

% E{p(y|x)}
for k = 1:K
    y_est  = y_est + beta_k(k, :) .* Mu_tild(:, :, k);
end

% Σ˜k
for k = 1:K
    Sigma_tild(:, :, k) = Sigma_yy(:, :, k) - Sigma_yx(:, :, k) * inv(Sigma_xx(:, :, k)) * Sigma_xy(:, :, k);
end

% Var{p(y|x)}
for m=1:M
    var_estRight = zeros(P);
    var_estLeft = zeros(P);
    for k=1:K
        var_estLeft = var_estLeft + beta_k(k, m) * (Mu_tild(:, m, k)^2 + Sigma_tild(:, :, k));
        var_estRight = var_estRight + beta_k(k, m) * Mu_tild(:, m, k); 
    end
    var_est(:, :, m) = var_estLeft - var_estRight^2;
end

end