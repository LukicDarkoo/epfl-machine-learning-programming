function [MSE, NMSE, Rsquared] = my_regression_metrics( yest, y )
%MY_REGRESSION_METRICS Computes the metrics (MSE, NMSE, R squared) for 
%   regression evaluation
%
%   input -----------------------------------------------------------------
%   
%       o yest  : (P x M), representing the estimated outputs of P-dimension
%       of the regressor corresponding to the M points of the dataset
%       o y     : (P x M), representing the M continuous labels of the M 
%       points. Each label has P dimensions.
%
%   output ----------------------------------------------------------------
%
%       o MSE       : (1 x 1), Mean Squared Error
%       o NMSE      : (1 x 1), Normalized Mean Squared Error
%       o R squared : (1 x 1), Coefficent of determination
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary Variables
[~, M] = size(y);
MSE = 0;
NMSE = 0;
Rsquared = 0;

y_mean = sum(y, 2) ./ M;
yest_mean = sum(yest, 2) ./ M;
var_y = 0;

% MSE
for m=1:M
    MSE = MSE + (sum(yest(:, m) - y(:, m)) .^ 2) / M;
end

% NMSE
for m=1:M
    var_y = var_y + (sum(y(:, m) - y_mean) .^ 2) / (M - 1);
end
NMSE = MSE / var_y;

% Rsquared
numer = 0;
denom_1 = 0;
denom_2 = 0;
for m=1:M
    numer = numer + (y(:, m) - y_mean) * (yest(:, m) - yest_mean);
    denom_1 = denom_1 + (y(:, m) - y_mean) ^ 2;
    denom_2 = denom_2 + (yest(:, m) - yest_mean) ^ 2;
end
Rsquared = (numer ^ 2) / (denom_1 * denom_2);

end

