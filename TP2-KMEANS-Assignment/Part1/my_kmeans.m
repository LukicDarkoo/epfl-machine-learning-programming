function [labels, Mu, Mu_init, iter] =  my_kmeans(X,K,init,type,MaxIter,plot_iter)
%MY_KMEANS Implementation of the k-means algorithm
%   for clustering.
%
%   input -----------------------------------------------------------------
%   
%       o X        : (N x M), a data set with M samples each being of dimension N.
%                           each column corresponds to a datapoint
%       o K        : (int), chosen K clusters
%       o init     : (string), type of initialization {'random','uniform'}
%       o type     : (string), type of distance {'L1','L2','LInf'}
%       o MaxIter  : (int), maximum number of iterations
%       o plot_iter: (bool), boolean to plot iterations or not (only works with 2d)
%
%   output ----------------------------------------------------------------
%
%       o labels   : (1 x M), a vector with predicted labels labels \in {1,..,k} 
%                   corresponding to the k-clusters.
%       o Mu       : (N x k), an Nxk matrix where the k-th column corresponds
%                          to the k-th centroid mu_k \in R^N 
%       o Mu_init  : (N x k), same as above, corresponds to the centroids used
%                            to initialize the algorithm
%       o iter     : (int), iteration where algorithm stopped
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Auxiliary Variable
[N, M] = size(X);
d_i    = zeros(K,M);
k_i    = zeros(1,M);
r_i    = zeros(K,M);
if plot_iter == [];plot_iter = 0;end

% Auxiliary Variable
[N, M] = size(X);
if plot_iter == [];plot_iter = 0;end

% Output Variables
labels  = zeros(1,M);
Mu      = zeros(N, K);
Mu_init = zeros(N, K);
iter      = 0;

% Step 1. Mu Initialization
Mu_init = kmeans_init(X, K, 'random');

%%%%%%%%%%%%%%%%%         TEMPLATE CODE      %%%%%%%%%%%%%%%%
% Visualize Initial Centroids if N=2 and plot_iter active
colors     = hsv(K);
if (N==2 && plot_iter)
    options.title       = sprintf('Initial Mu with <%s> method', init);
    ml_plot_data(X',options); hold on;
    ml_plot_centroid(Mu_init',colors);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Mu = Mu_init;
Mu_prev = zeros(size(Mu));
iter = 0;
while iter < MaxIter
    
    %%%%% Implement K-Means Algorithm HERE %%%%%    
    Mu_prev = Mu;
    
    % Step 2. Distances from X to Mu
	d = my_distX2Mu(X, Mu, type);
    
	% Step 3. Assignment Step: Mu Responsability
	% Equation 5 and 6	
    [~, i] = min(d, [], 1);
    labels = i;
    % assignin('base', 'X', X)
    % assignin('base', 'd', d)
    % assignin('base', 'Mu', Mu)
    for i_k=1:K
        for i_M=1:M
            r_i(i_k, i_M) = (i(i_M) == i_k);
        end
    end
	
	% Step 4. Update Step: Recompute Mu	
    for i_k=1:K
        for i_N=1:N
            Mu(i_N, i_k) = sum(r_i(i_k, :) .* X(i_N, :)) / sum(r_i(i_k, :));
        end
    end
    
    
	% Check for stopping conditions (Mu stabilization or MaxIter)
	if Mu_prev == Mu
        break
    end
    
    %%%%%%%%%%%%%%%%%         TEMPLATE CODE      %%%%%%%%%%%%%%%%       
%     if (N==2 && iter == 1 && plot_iter)
%         options.labels      = labels;
%         options.title       = sprintf('Mu and labels after 1st iter');
%         ml_plot_data(X',options); hold on;
%         ml_plot_centroid(Mu',colors);
%     end    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    iter = iter+1;
end


%%%%%%%%%%%   TEMPLATE CODE %%%%%%%%%%%%%%%
if (N==2 && plot_iter)
    options.labels      = labels;
    options.class_names = {};
    options.title       = sprintf('Mu and labels after %d iter', iter);
    ml_plot_data(X',options); hold on;    
    ml_plot_centroid(Mu',colors);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
