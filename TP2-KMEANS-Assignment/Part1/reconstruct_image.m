function [rimg] = reconstruct_image(labels,centroids,imgSize)
%RECONSTRUCT_IMAGE Reconstruct the image given the labels and the centroids
%
%   input -----------------------------------------------------------------
%   
%       o labels: The labels of the corresponding centroid for each pixel
%       o centroids: All the centroids and their RGB color value
%       o imgSize: Size of the original image for reconstruction
%
%   output ----------------------------------------------------------------
%
%       o rimg : The reconstructed image

% ADD CODE HERE: Reconstruct the image based on the labels on the centroids
% HINT: Apply the two steps you have used to reshape in the opposite order 
% if necessary

[~, WH] = size(labels);
[nColors, K] = size(centroids);

rimg = zeros(nColors, WH);
for iColor=1:nColors
    for iSample=1:WH
        rimg(iColor, iSample) = centroids(iColor, labels(iSample));
    end
end

rimg = rimg';
rimg = reshape(rimg, [imgSize(1), imgSize(2), nColors]);

% END CODE
end
