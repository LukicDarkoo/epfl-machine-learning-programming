function [F1_overall, P, R, F1] =  my_f1measure(cluster_labels, class_labels)
%MY_F1MEASURE Computes the f1-measure for semi-supervised clustering
%
%   input -----------------------------------------------------------------
%   
%       o class_labels     : (N x 1),  N-dimensional vector with true class
%                                       labels for each data point
%       o cluster_labels   : (N x 1),  N-dimensional vector with predicted 
%                                       cluster labels for each data point
%   output ----------------------------------------------------------------
%
%       o F1_overall      : (1 x 1)     f1-measure for the clustered labels
%       o P               : (nClusters x nClasses)  Precision values
%       o R               : (nClusters x nClasses)  Recall values
%       o F1              : (nClusters x nClasses)  F1 values
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Auxiliary Variables
M         = length(class_labels);
true_K    = unique(class_labels);
found_K   = unique(cluster_labels);
nClasses  = length(true_K);
nClusters = length(found_K);

% Output Variables
P = zeros(nClusters, nClasses);
R = zeros(nClusters, nClasses);
F1 = zeros(nClusters, nClasses);
F1_overall = 0;

for C_i=1:nClasses
    for k=1:nClusters
        n_ik = sum(class_labels == true_K(C_i) & cluster_labels == found_K(k));
        P(k, C_i) = n_ik / sum(found_K(k) == cluster_labels);
        R(k, C_i) = n_ik / sum(true_K(C_i) == class_labels);
        
        F1(k, C_i) = (2 * R(k, C_i) * P(k, C_i)) / (R(k, C_i) + P(k, C_i) + eps(1));
    end
end

for C_i=1:nClasses
    k = max(F1(:, C_i));
    abs_c_i = sum(true_K(C_i) == class_labels);
    F1_overall = F1_overall + (abs_c_i / M) * k;
end

end
